set-location $PSScriptRoot
return;


# Verbs
Get-Verb

# Non normed verb example
Import-Module .\z_verbsModule.psm1
NotNormed-Function

# Help example
Get-Help Get-ChildItem

# Commands
Get-Command *-Service
Get-Command Test-*

# Modules
Get-Module 
Get-Module -ListAvailable

# Providers
Import-Module "ActiveDirectory"
cd "AD:\OU=Hosting,DC=D,DC=ETHZ,DC=CH"
cd "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate"
cd "Alias:\"
