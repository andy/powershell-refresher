# Install the IAMClient
Install-Module IAMClient

# And install the speculationcontrol module
Install-Module speculationcontrol

# After it's done, we can start using the code
# !!! Does not work correctly in ps core !!!
Get-SpeculationControlSettings

# If a module needs an update, use:
Update-Module